import { IEmployee } from 'app/shared/model/employee.model';

export interface ITask {
  id?: number;
  name?: string;
  description?: string;
  estimation?: number;
  employees?: IEmployee[];
  projectId?: number;
}

export const defaultValue: Readonly<ITask> = {};
