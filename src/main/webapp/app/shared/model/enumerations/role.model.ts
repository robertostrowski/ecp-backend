export const enum Role {
  ADMINISTRATOR,
  MANAGER,
  WORKER
}
