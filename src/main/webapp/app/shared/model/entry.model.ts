import { Moment } from 'moment';

export interface IEntry {
  id?: number;
  date?: Moment;
  worktime?: number;
  comment?: string;
  taskId?: number;
  employeeId?: number;
}

export const defaultValue: Readonly<IEntry> = {};
