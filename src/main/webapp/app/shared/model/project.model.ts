import { ITask } from 'app/shared/model/task.model';
import { IEmployee } from 'app/shared/model/employee.model';

export interface IProject {
  id?: number;
  name?: string;
  tasks?: ITask[];
  employees?: IEmployee[];
}

export const defaultValue: Readonly<IProject> = {};
