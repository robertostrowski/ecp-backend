import { IEntry } from 'app/shared/model/entry.model';
import { IProject } from 'app/shared/model/project.model';
import { ITask } from 'app/shared/model/task.model';
import { Role } from 'app/shared/model/enumerations/role.model';

export interface IEmployee {
  id?: number;
  name?: string;
  surname?: string;
  email?: string;
  role?: Role;
  entries?: IEntry[];
  projects?: IProject[];
  tasks?: ITask[];
}

export const defaultValue: Readonly<IEmployee> = {};
