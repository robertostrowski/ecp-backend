package szmiro.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    ADMINISTRATOR, MANAGER, WORKER
}
