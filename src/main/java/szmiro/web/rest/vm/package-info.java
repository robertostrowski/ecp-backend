/**
 * View Models used by Spring MVC REST controllers.
 */
package szmiro.web.rest.vm;
