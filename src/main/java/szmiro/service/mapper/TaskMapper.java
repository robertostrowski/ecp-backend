package szmiro.service.mapper;


import szmiro.domain.*;
import szmiro.service.dto.TaskDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Task} and its DTO {@link TaskDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, ProjectMapper.class})
public interface TaskMapper extends EntityMapper<TaskDTO, Task> {

    @Mapping(source = "project.id", target = "projectId")
    TaskDTO toDto(Task task);

    @Mapping(target = "removeEmployee", ignore = true)
    @Mapping(source = "projectId", target = "project")
    Task toEntity(TaskDTO taskDTO);

    default Task fromId(Long id) {
        if (id == null) {
            return null;
        }
        Task task = new Task();
        task.setId(id);
        return task;
    }
}
