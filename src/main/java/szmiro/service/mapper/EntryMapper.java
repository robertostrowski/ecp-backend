package szmiro.service.mapper;


import szmiro.domain.*;
import szmiro.service.dto.EntryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Entry} and its DTO {@link EntryDTO}.
 */
@Mapper(componentModel = "spring", uses = {TaskMapper.class, EmployeeMapper.class})
public interface EntryMapper extends EntityMapper<EntryDTO, Entry> {

    @Mapping(source = "task.id", target = "taskId")
    @Mapping(source = "employee.id", target = "employeeId")
    EntryDTO toDto(Entry entry);

    @Mapping(source = "taskId", target = "task")
    @Mapping(source = "employeeId", target = "employee")
    Entry toEntity(EntryDTO entryDTO);

    default Entry fromId(Long id) {
        if (id == null) {
            return null;
        }
        Entry entry = new Entry();
        entry.setId(id);
        return entry;
    }
}
