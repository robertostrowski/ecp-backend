package szmiro.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link szmiro.domain.Entry} entity.
 */
public class EntryDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant date;

    @NotNull
    private Float worktime;

    private String comment;


    private Long taskId;

    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Float getWorktime() {
        return worktime;
    }

    public void setWorktime(Float worktime) {
        this.worktime = worktime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EntryDTO entryDTO = (EntryDTO) o;
        if (entryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EntryDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", worktime=" + getWorktime() +
            ", comment='" + getComment() + "'" +
            ", taskId=" + getTaskId() +
            ", employeeId=" + getEmployeeId() +
            "}";
    }
}
